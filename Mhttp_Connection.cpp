// Mhttp_Connection.cpp
//  Implementation of Mhttp::Connection class.
#include <iostream>
#include <sstream>
#include <cassert>
#include <unordered_map>
#include <string>
#include <limits>

#include <boost/asio.hpp>

#include "Mhttp_Connection.h"
#include "helpers.h"

namespace asio = boost::asio;
using boost::system::error_code;

namespace Mhttp {
    // m_request_command_lookup - hashtable used to convert the HTTP request string into the enum value.
    const std::unordered_map<std::string, Connection::Request::Type> Connection::m_request_command_lookup {
        {"GET", Request::Type::GET},
        {"POST", Request::Type::POST},
        {"DELETE", Request::Type::DEL}
    };

    // Connection::Connection - default constructor
    Connection::Connection(tcp::socket socket, IDataStore& storage) :
        m_socket {std::move(socket)},
        m_storage {storage}
    {
        // added this and the one in the destructor, so you could see when the connection objects were constructed and destructed
        // they helped to diagnose and fix some lifetime issues with the Connection objects. It continues to be useful so I kept them
        // around.
        std::cout << "Connection constructed. " << m_socket.remote_endpoint() << "\n"; 
    }

    // Connection::~Connection - destructor
    Connection::~Connection() {
        std::cout << "Connection deconstructed. " << m_socket.remote_endpoint() << "\n";
    }

    // Connection::start - starts a connection, called by the lamda in Server::do_accept
    void Connection::start() {
        read_request_command();
    }

    // read_request_command - reads the first line of the http request. 
    void Connection::read_request_command() {
        // starts an async read of the first line.  The lamda handles parsing and starting the next step after the read completes.
        auto self {shared_from_this()};
        asio::async_read_until(m_socket, m_buffer, "\r\n",
            [this, self](const error_code &ec, std::size_t size) {
                if( ec ) {
                    std::cerr << "Error receiving HTTP request " << ec << "\n";
                    return;
                }

                std::istream buffer_in(&m_buffer);
                std::string http_request, uri, http_version;
                std::string request_line;
                std::getline(buffer_in, request_line, '\r');
                buffer_in.ignore(2, '\n'); // ignore throught line feed still in the buffer
                std::istringstream iss {request_line};
                iss >> http_request >> uri >> http_version;

                // Likely should support other HTTP versions.
                if( http_version.empty() ) {
                    write_response({505, "HTTP version missing"});
                    return;
                } else if( http_version != "HTTP/1.1" ) {
                    write_response({505, "HTTP version not supported"});
                    return;
                }

                // return 400 error if the http_request is missing or we get a 
                // http request that we don't know how to handle
                auto lookup_iter = m_request_command_lookup.find(http_request);
                if( lookup_iter == m_request_command_lookup.end() ) {
                    write_response({400, "Bad request"});
                    return;
                }

                // check for missing uri
                if( uri.empty() ) {
                    write_response({400, "Missing uri"});
                    return;
                }

                // production code would validate the URI further, but for the purposes of this
                // interview project, it'll just accept any string as valid
                std::cout << "Received " << lookup_iter->first << " command. URI=" << uri << "\n";

                m_request.command = lookup_iter->second;
                auto parse_result = parse_uri(uri);
                m_request.uri = std::move(parse_result.first);
                m_request.version = parse_result.second;

                read_request_headers();  // move to the next step
            });
    }

    // read_request_headers - reads all of the headers into m_request.headers
    void Connection::read_request_headers() {
        // starts an async read until the blank line after all the headers, the lamda handles the parsing after the read is done.
        auto self {shared_from_this()};
        asio::async_read_until(m_socket, m_buffer, "\r\n\r\n",
            [this, self](const error_code &ec, std::size_t size) {
                if( ec ) {
                    std::cerr << "Error receiving HTTP headers " << ec << "\n";
                    return;
                }

                std::istream buffer_in(&m_buffer);
                std::string header_line;
                for(;;) {
                    // get each line up to the CR, then ignore through the LF, max of 2 chars CR + LF
                    std::getline(buffer_in, header_line, '\r');
                    buffer_in.ignore(2, '\n');
                    if(header_line.empty()) break;  // empty line ends the loop

                    // parse the header line
                    auto colon_pos = header_line.find(':');
                    if( colon_pos == std::string::npos ) {
                        write_response({400, "Invalid header line"});
                        return;
                    }
                    auto header_name = header_line.substr(0, colon_pos);
                    auto header_value = header_line.substr(header_line.find_first_not_of(' ', colon_pos + 1));

                    // print it and add to the hash table
                    std::cout << "Received header: name=" << header_name << ", value=" << header_value << "\n";
                    m_request.headers[header_name] = header_value;
                }

                read_request_body();  // move to the next step
            });
    }

    // read_request_body - Reads the body of the request if it is present as signaled by the Content-Length header.
    void Connection::read_request_body() {
        // if there's no Content-Length header, assume body is empty
        auto iter = m_request.headers.find("Content-Length");
        if( iter != m_request.headers.end() ) {
            size_t remaining_size = parse_number(iter->second) - m_buffer.size();
            // starts an async read to get the remaining bytes in the content (some may have already been read into the buffer),
            // the lamda just copies the data out of the buffer into m_request.body
            auto self {shared_from_this()};
            asio::async_read(m_socket, m_buffer, asio::transfer_exactly(remaining_size),
                [this, self](const error_code &ec, std::size_t size) {
                    if( ec ) {
                        std::cerr << "Error reading request body " << ec << "\n";
                        return;
                    }

                    std::string body(asio::buffers_begin(m_buffer.data()), asio::buffers_end(m_buffer.data()));
                    m_buffer.consume(size);
                    std::cout << "Received body of " << body.size() << " bytes.\n";
                    m_request.body = std::move(body);

                    dispatch_request(); // move to the next step
                });
        } else {
            dispatch_request(); // move to the next step when there is no body to read
        }
    }

    // dispatch_request - run the appropriate method based on the type of the http request
    void Connection::dispatch_request() {
        switch(m_request.command) {
            case Request::GET:
                handle_get_request();
                break;
            case Request::POST:
                handle_post_request();
                break;
            case Request::DEL:
                handle_delete_request();
                break;
        }
    }

    // handle_get_request - respond with the uri content from the DataStore if it exists, if not respond with 404
    void Connection::handle_get_request() {
        auto content = m_storage.get(m_request.uri, m_request.version);
        if( !content.empty() ) {
            write_response({200, "OK", content.body, content.content_type});
        } else {
            write_response({404, "Not Found"});
        }
    }

    // handle_post_request - associate the content body and type with the uri and add to the DataStore.
    void Connection::handle_post_request() {
        auto iter = m_request.headers.find("Content-Type");
        if( iter == m_request.headers.end() ) {
            write_response({400, "Bad Request. No Content-Type"});
        } else {
            auto version = m_storage.put(m_request.uri, {iter->second, std::move(m_request.body)});
            write_response({200, "OK", version});
        }
    }

    // handle_delete_request - remove the uri's content from the DataStore.
    void Connection::handle_delete_request() {
        auto version = m_storage.put(m_request.uri);
        write_response({200, "OK", version});
    }

    // write_response - write the http response out to the socket, connection ends after the write and the connection object is destroyed
    void Connection::write_response(const Response& response) {
        // start an async write of the response, Connection object is destroyed after the lamda returns
        auto self {shared_from_this()};
        asio::async_write(m_socket, asio::buffer(response.str()),
            [this, self](const error_code&ec, std::size_t size) {
                if( ec ) {
                    std::cerr << "Error sending response\n";
                } else {
                    std::cout << "Sent response, size = " << size << "\n";
                }
            }
        );
    }

    // Response::str() - build and return the http response string
    std::string Connection::Response::str() const {
        // create the response headers and body
        std::ostringstream oss;
        oss << "HTTP/1.1 " << m_code << " " << m_description << "\r\n";
        if (!m_body.empty()) {
            oss << "Content-Type: " << m_content_type << "\r\n"
                << "Content-Length: " << m_body.size() << "\r\n";
        }
        if (m_version != 0) {
            oss << "X-Data-Version: " << m_version << "\r\n";
        }
        oss << "\r\n";  // must include a blank line after the header. http uses CRLF as the line seperator
        if (!m_body.empty()) {
            oss << m_body;
        }

        return oss.str();
    }
}