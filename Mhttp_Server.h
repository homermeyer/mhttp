// Mhttp_Server.h
//  Declaration of Mhttp::Server class
#ifndef MHTTP_SERVER_H_
#define MHTTP_SERVER_H_

#include <boost/asio.hpp>

#include "IDataStore.h"

namespace Mhttp {
    using boost::asio::ip::tcp;

    class Server
    {
    public:
        Server(boost::asio::io_context& ioc, unsigned short port, IDataStore& storage);

    private:
        void do_accept();

        tcp::acceptor m_acceptor;
        IDataStore& m_storage;
    };
}

#endif