// helpers.cpp
//  Implementation of functions and classes declared in helpers.h
#include <sstream>
#include <limits>
#include <locale>
#include <stdexcept>
#include <cstring>

#include "helpers.h"

// parse_number
//  Throws:
//		std::invalid_argument if str has any non-digit characters
//	Returns:
//		The number represented by str as an unsigned long.
//		Uses strtoul to do the conversion. Out of range values are handled by either returning the min or max unsigned long value.
unsigned long parse_number(const std::string& str) {
    // first verify that all characters are digits
    std::locale c_locale;  // use the default C locale
    for( auto ch : str ) {
        if( !std::isdigit(ch, c_locale) ) {
            throw std::invalid_argument {"only digits between 0 and 9 are allowed"};
        }
    }
	return std::strtoul(str.c_str(), nullptr, 10);
}

// parse_port
//	Throws:
//		std::invalid_argument if str has any non-digit characters or is out of range for a tcp/ip port number.
//	Returns:
//		The number represented by str as an unsigned long.
unsigned short parse_port(const std::string& port_str) {
	// verify the value is in the range of an unsigned short
	auto port_value = parse_number(port_str);
	using ushort_limits = std::numeric_limits<unsigned short>;
	if( port_value > ushort_limits::max() ) {
		std::ostringstream oss;
		oss << "Port number must be between " 
			<< ushort_limits::min() << " and " << ushort_limits::max();
		throw std::invalid_argument {oss.str()};
	}
	return static_cast<unsigned short>(port_value);
}

// parse_uri
// 	Returns:
//		std::pair
//			first - the plain uri with all the parameters removed
//			second - value of the version paramter if it exists and has a valid number
std::pair<std::string, unsigned int> parse_uri(const std::string &uri) {
	auto question_pos = uri.find('?');
	if( question_pos == std::string::npos ) {
		// no parameters, return whole uri, version is 0
		return std::make_pair(uri, 0U);
	}
	auto plain_uri = uri.substr(0, question_pos);
	const std::string version_id = "?version=";
	auto version_pos = uri.find(version_id, question_pos);
	if( version_pos == std::string::npos ) {
		// no version in the parameters, return uri with parameters removed, version is 0
		return std::make_pair(plain_uri, 0U);
	} else {
		auto digit_start = version_pos + version_id.size();
		auto digit_end = uri.find_first_not_of("0123456789", digit_start);
		auto digit_length = digit_end == std::string::npos ? digit_end : digit_end - digit_start;
		if( digit_length == 0 ) {
			// version parameter exists, but there's no valid number
			// return uri with parameters removed, version is 0
			return std::make_pair(plain_uri, 0U);
		}
		auto version = parse_number(uri.substr(digit_start, digit_length)); // will not throw, we already verified that it's all digits
		return std::make_pair(plain_uri, version);
	}
}

