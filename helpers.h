// helpers.h
//  Definitions of miscellaneous helper functions that don't fit in another file.
#ifndef HELPERS_H_
#define HELPERS_H_

#include <string>
#include <utility>

unsigned long parse_number(const std::string &str);
unsigned short parse_port(const std::string &str);
std::pair<std::string, unsigned int> parse_uri(const std::string &uri);

#endif