// mersive_http.cpp
//	Implementation of the main function.
#include <iostream>
#include <exception>

#include <boost/asio.hpp>

#include "Mhttp_Server.h"
#include "helpers.h"
#include "MemoryDataStore.h"

using boost::asio::ip::tcp;
namespace asio = boost::asio;

int main(int argc, char* argv[]) {
	try {
		if (argc != 2) {
			std::cerr << "Usage: " << argv[0] << " <port>\n";
			return 1;
		}

		asio::io_context io_context;
		MemoryDataStore memory_data;
		Mhttp::Server mhttp_server {io_context, parse_port(argv[1]), memory_data};
        io_context.run();
	}
	catch( std::exception& e ) {
		std::cerr << "Exception thrown: " << e.what() << "\n";
	}
	catch( ... ) {
		std::cerr << "Uknown Exception thrown\n";
	}
}