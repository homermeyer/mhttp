// MemoryDataStore.cpp
//  Implementation of concrete IDataStore classes.

#include "MemoryDataStore.h"

MemoryDataStore::~MemoryDataStore() {
}

ContentRecord MemoryDataStore::get(const std::string& key, unsigned int version) {
    auto iter = m_storage.find(key);
    if( iter != m_storage.end() ) {
        if( iter->second.size() == 0 || version > iter->second.size() ) {
            // key exists, but version requested does not
            return {};
        } else {
            // version of 0 means the last element in the vector
            if( version == 0 ) version = iter->second.size();
            return iter->second[version - 1];
        }
    } else {
        // no matching key in the hashtable
        return {};
    }
}

unsigned int MemoryDataStore::put(const std::string& key, ContentRecord value) {
    auto& container = m_storage[key]; // get existing or insert empty vector if key doesn't exist
    container.emplace_back(std::move(value));
    return container.size(); // the new size happens to be the new version number :-)
}