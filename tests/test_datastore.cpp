#include "catch.hpp"
#include "../MemoryDataStore.h"

// helper comparison function for ContentRecord to ease test implementation
bool are_equal(const ContentRecord& lhs, const ContentRecord&& rhs) {
	return lhs.content_type == rhs.content_type && lhs.body == rhs.body;
}

TEST_CASE("Tests for MemoryDataStore class", "[DataStore]") {
	MemoryDataStore storage;

	REQUIRE(storage.get("abc").empty());
	REQUIRE(storage.put("abc", { "text/plain", "body is here"}) == 1);
	REQUIRE(are_equal(storage.get("abc"), {"text/plain", "body is here"}));
	REQUIRE(storage.put("abc", { "monty/python", "something completely different" }) == 2);
	REQUIRE(are_equal(storage.get("abc"), {"monty/python", "something completely different"}));
	REQUIRE(storage.put("abc") == 3); // this is a remove operation
	REQUIRE(storage.get("abc").empty());
	REQUIRE(are_equal(storage.get("abc", 1), {"text/plain", "body is here"}));
	REQUIRE(are_equal(storage.get("abc", 2), {"monty/python", "something completely different"}));
	REQUIRE(storage.get("abc", 3).empty());
}