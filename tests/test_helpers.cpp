#include "catch.hpp"
#include "../helpers.h"

TEST_CASE("Unit tests for helper functions", "[helpers]") {
	REQUIRE(parse_number("123") == 123);
	REQUIRE_THROWS_AS(parse_number("-1"), std::invalid_argument);
	REQUIRE_THROWS_AS(parse_number("abc"), std::invalid_argument);

	REQUIRE(parse_port("8000") == 8000);
	REQUIRE_THROWS_AS(parse_port("65537"), std::invalid_argument);

	using result_t = std::pair<std::string, unsigned int>;
	REQUIRE(parse_uri("/foo") == result_t("/foo", 0));
	REQUIRE(parse_uri("/foo?one=1?two=2?three=3") == result_t("/foo",0));
	REQUIRE(parse_uri("/foo?one=1?version=abc?two=2") == result_t("/foo",0));
	REQUIRE(parse_uri("/foo?one=1?version=?two=2") == result_t("/foo",0));
	REQUIRE(parse_uri("/foo?version=1") == result_t("/foo",1));
	REQUIRE(parse_uri("/foo?one=1?version=2?three=3") == result_t("/foo",2));
	REQUIRE(parse_uri("/foo?one=1?version=2") == result_t("/foo",2));
}