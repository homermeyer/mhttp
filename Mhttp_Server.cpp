// Mhttp_Server.cpp
//  Implementation of Mhttp::Server class
#include <iostream>
#include <memory>

#include <boost/asio.hpp>

#include "Mhttp_Server.h"
#include "Mhttp_Connection.h"

namespace Mhttp {
    // default constructor
    Server::Server(boost::asio::io_context& ioc, unsigned short port, IDataStore& storage) :
        m_acceptor {ioc, tcp::endpoint(tcp::v4(), port)},
        m_storage {storage}
    {
        std::cout << "Constructed Server listening on port " << port << "\n";
        do_accept(); // start listening for connections immediately
    }

    void Server::do_accept() {
        // start an async accept operation and return immediately.  
        // The lamda runs after a connection is accept, creates a Connection object for each one
        m_acceptor.async_accept(
            [this](boost::system::error_code ec, tcp::socket socket) {
                if( !ec ) {
                    std::cout << "Accepted connection from " << socket.remote_endpoint() << "\n";
                    std::make_shared<Connection>(std::move(socket), m_storage)->start();
                } else {
                    std::cerr << "Accept failed with error " << ec << "\n";
                }
                do_accept(); // listen for another connection. Note this isn't recursion. The original do_accept call has already
                             // exited by the time this lamda executes.
            });
    } 
}