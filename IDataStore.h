// IDataStore.h
//  IDataStore interface declaration
#ifndef DATASTORE_H_
#define DATASTORE_H_

#include <string>
#include <unordered_map>
#include <vector>

struct ContentRecord {
    std::string content_type;
    std::string body;

    bool empty() const { return content_type.empty() && body.empty(); }
};

struct IDataStore {
    virtual ~IDataStore() {};

    // IDataStore::get
    //  returns the ContentRecord associated with the key and version (0 means latest version) if it exists.
    //  If it does not exist, it returns an empty ContentRecord without adding anything to the DataStore
    virtual ContentRecord get(const std::string& key, unsigned int version = 0) = 0;

    // IDataStore::put
    //  associates value with key and adds it to the DataStore. returns the version of the new record.
    virtual unsigned int put(const std::string& key, ContentRecord value = {}) = 0;
};

#endif