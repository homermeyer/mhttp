// Mhttp_Connection.h
//  Declaration of Mhttp::Connection class
#ifndef MHTTP_CONNECTION_H_
#define MHTTP_CONNECTION_H_

#include <memory>
#include <string>
#include <unordered_map>
#include <utility>

#include <boost/asio.hpp>

#include "IDataStore.h"

namespace Mhttp {
    using boost::asio::ip::tcp;

    class Connection : 
        public std::enable_shared_from_this<Connection>
    {
    public:
        Connection(tcp::socket socket, IDataStore& storage);
        ~Connection();
        void start();

    private:
        void read_request_command();
        void read_request_headers();
        void read_request_body();
        void dispatch_request();
        void handle_get_request();
        void handle_post_request();
        void handle_delete_request();

        struct Request {
            enum Type {GET, POST, DEL} command;
            std::string uri;
            unsigned int version = 0;
            std::unordered_map<std::string, std::string> headers;
            std::string body;
        } m_request;

        class Response {
            public:
                // some trivial constructors
                Response(unsigned int code, const std::string& description) :
                    m_code{code}, m_description{description} 
                {}

                Response(
                    unsigned int code, const std::string& description, const std::string& body, 
                    const std::string& content_type = "text/plain"
                ) :
                    m_code{code}, m_description{description}, m_body{body}, 
                    m_content_type{content_type} 
                {}

                Response(unsigned int code, const std::string& description, unsigned int version) :
                    m_code{code}, m_description{description}, m_version{version} 
                {}

            std::string str() const; // build and return the response string

            private:
                unsigned int m_code;
                std::string m_description;
                unsigned int m_version = 0;
                std::string m_content_type;
                std::string m_body;
        };

        void write_response( const Response& response );

        static const std::unordered_map<std::string, Request::Type> m_request_command_lookup;

        tcp::socket m_socket;
        boost::asio::streambuf m_buffer;
        IDataStore& m_storage;
    };
}

#endif