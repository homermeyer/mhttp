// MemoryDataStore.h
//  MemoryDataStore declaration. Implements IDataStore to store the ContentRecords in memory.
#ifndef MEMORYDATASTORE_H_
#define MEMORYDATASTORE_H_

#include "IDataStore.h"

class MemoryDataStore : public IDataStore {
    public:
        virtual ~MemoryDataStore();

        virtual ContentRecord get(const std::string& key, unsigned int version = 0);
        virtual unsigned int put(const std::string& key, ContentRecord value = {});
    
    private:
        std::unordered_map<std::string, std::vector<ContentRecord>> m_storage;
};

#endif